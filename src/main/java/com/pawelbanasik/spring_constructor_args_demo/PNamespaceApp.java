package com.pawelbanasik.spring_constructor_args_demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.pawelbanasik.domain.Organization;

public class PNamespaceApp {
	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
		
		Organization organization = (Organization) context.getBean("myorg");
		
		System.out.println(organization);
		
		((AbstractApplicationContext) context).close();
		
	}
}
